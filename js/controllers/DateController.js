Application.DateController = Ember.ObjectController.extend({
  today: [
    new Date().getFullYear(),
    new Date().getMonth() + 1,
    new Date().getDate(),
  ],

  selectedDay: null,

  monthes: [
    "January",
    "Febroary", 
    "March", 
    "April", 
    "May", 
    "June", 
    "July", 
    "August", 
    "September", 
    "October", 
    "November", 
    "December"
  ],
  getMonth: function() {
    return this.monthes[this.get('model.month') - 1];
  }.property('@each'),

  getDays: function() {
    var days = new Array();//массив номеров дней в месяце
    var date = new Date();
    var dayOfWeek = new Date(this.get('model.year'), this.get('model.month') - 1, 1).getDay();//день недели первого числа текущего года и текущего месяца
    var lastDay = new Date(this.get('model.year'),this.get('model.month'),0).getDate();//последний день текущего года и текущего месяца
    
    for(var i = 0; i < dayOfWeek; i++)
    {
      var o = new Object();
      o.value = ' ';
      o.isToday = false;
      o.isSelected = false;
      days[i] = o;  
    }

    var j = dayOfWeek;
    for (var i = 0; i < lastDay; i++) {
      var o = new Object();
      o.value = i + 1;
      
      if ((i + 1) == this.today[2] && this.get('model.month') == this.today[1] && this.get('model.year') == this.today[0])
        o.isToday = true;
      else
        o.isToday = false;

      if (this.selectedDay == (i + 1))
        o.isSelected = true;
      else
        o.isSelected = false;
      days[j] = o;
      j++;
    };

    if (this.selectedDay > lastDay)
    {
      days[days.length - 1].isSelected = true;
      this.set('this.selectedDay', lastDay);
    }

    var weeks = new Array();
    weeks[0] = days.slice(0, 7);
    weeks[1] = days.slice(7, 14);
    weeks[2] = days.slice(14, 21);
    weeks[3] = days.slice(21, 28);
    weeks[4] = days.slice(28, 35);
    weeks[5] = days.slice(35, days.length);
    
    return weeks;
  }.property('@each'),

  actions: {
    today: function() {
      var date = new Date();
      this.transitionToRoute('date', {year: date.getFullYear(), month: date.getMonth() + 1, day: date.getDate()});
    },

    nextYear: function() {
      this.transitionToRoute('date', {year: Number(this.get('model.year')) + 1, month: Number(this.get('model.month')), day: Number(this.get('model.day'))});
    },

    prevYear: function() {
      this.transitionToRoute('date', {year: Number(this.get('model.year')) - 1, month: Number(this.get('model.month')), day: Number(this.get('model.day'))});
    },

    nextMonth: function() {
      if (this.get('model.month') == 12)
        this.transitionToRoute('date', {year: Number(this.get('model.year')) + 1, month: 1, day: Number(this.get('model.day'))});
      else
        this.transitionToRoute('date', {year: Number(this.get('model.year')), month: Number(this.get('model.month')) + 1, day: Number(this.get('model.day'))});
    },

    prevMonth: function() {
      if (this.get('model.month') == 1)
        this.transitionToRoute('date', {year: Number(this.get('model.year')) - 1, month: 12, day: Number(this.get('model.day'))});
      else
        this.transitionToRoute('date', {year: Number(this.get('model.year')), month: Number(this.get('model.month')) - 1, day: Number(this.get('model.day'))});
    },

    selectedDate: function(day) {
      this.selectedDay = day;
      this.transitionToRoute('date', {year: this.get('model.year'), month: this.get('model.month'), day: day});
    }
  }
});