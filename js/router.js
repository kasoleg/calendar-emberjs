Application.Router.map(function(){
  this.route('index', {path: '/'});
  this.route('date', {path: 'year/:year/month/:month/day/:day'});
});

Application.IndexRoute = Ember.Route.extend({
  model: function  () {
    return{
        year: new Date().getFullYear(),
        month: new Date().getMonth() + 1,
        day: new Date().getDate()
    }
  },

  afterModel: function(model) {
    this.transitionTo('date', model);
  }  
});
